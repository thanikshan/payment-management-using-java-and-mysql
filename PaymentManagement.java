//package Assignment6;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PaymentManagement {

	private ArrayList<Integer> unpaidOrdersList;
	private ArrayList<String> unUsedChequeList;
	private Statement statement = null;
	private ResultSet resultSet = null;
	private String schema = "use tshanmugam;";
	private boolean resultChecker;
	private int modifiedRows;
	private int rowCount;
	private ArrayList<Order> orderList;
	Scanner in = new Scanner(System.in);
	Order order;
	private int checkLoop = 0;

	//method to run select query
	public ResultSet queryExecutor(String query) throws SQLException {
		ResultSet temp = statement.executeQuery(query);
		return temp;
	}

	//method to run update query
	public int dataModifier(String query) {
		try {
			int modifiedRows = statement.executeUpdate(query);
			return modifiedRows;
		} catch (SQLException e) {
			return 0;
		}
	}
	//method to alter the column 
	public Boolean columnModifier(Connection database, String query) {
		try {
			Statement temp = database.createStatement();
			temp.executeUpdate(query);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	//method to sync the orders and payment
	void updatePayments() {
		System.out.println("Syncing data.......................................");
		String sync = "update payments, (select orders.orderNumber , sum(orderdetails.priceEach *  orderdetails.quantityOrdered) as amount,orders.customerNumber, orders.status from orders inner join  orderdetails on orders.orderNumber = orderdetails.orderNumber group by orders.orderNumber ) temp set payments.orderNumber = temp.orderNumber where payments.customerNumber = temp.customerNumber and payments.amount = temp.amount;";
		modifiedRows = dataModifier(sync);
		if (modifiedRows > 0) {
			System.out.println("Sync completed successfully");
			System.out.println(modifiedRows + "rows updated");
		}

	}
	//method to update the payment status in orders for easy reference
	void updatePaymentStatus() {
		System.out.println("Syncing data.......................................");
		String syncOrderStatus = "update orders, (select totalOrders.customerNumber , totalOrders.orderNumber , totalOrders.status, totalOrders.amount , payments.paymentDate , payments.checkNumber from (select orders.orderNumber , sum(orderdetails.priceEach *  orderdetails.quantityOrdered) as amount,orders.customerNumber, orders.status from orders  inner join  orderdetails on orders.orderNumber = orderdetails.orderNumber group by orders.orderNumber order by orders.customerNumber) as totalOrders inner join payments on totalOrders.amount = payments.amount ) t1 set orders.paymentStatus = 'paid' where orders.customerNumber = t1.customerNumber and orders.OrderNumber = t1.orderNumber ;";
		modifiedRows = dataModifier(syncOrderStatus);
		String syncUnpaidOrders = "update orders, (select totalOrders.customerNumber , totalOrders.orderNumber , totalOrders.status, totalOrders.amount , payments.paymentDate , payments.checkNumber from (select orders.orderNumber , sum(orderdetails.priceEach *  orderdetails.quantityOrdered) as amount,orders.customerNumber, orders.status from orders  inner join  orderdetails on orders.orderNumber = orderdetails.orderNumber group by orders.orderNumber order by orders.customerNumber) as totalOrders left join payments on totalOrders.amount = payments.amount  where   payments.paymentDate is null) t1 set orders.paymentStatus = 'unpaid' where orders.customerNumber = t1.customerNumber and orders.OrderNumber = t1.orderNumber ;";
		int unpaidOrders = dataModifier(syncUnpaidOrders);
		if ((modifiedRows > 0) || (unpaidOrders > 0)) {
			System.out.println("Sync completed successfully");
			System.out.println(modifiedRows + " rows updated as Paid");
			System.out.println(unpaidOrders + " rows updated as unpaid");

		}

	}

	//Can be used when there are multiple schemas available
	void setSchema(Connection database) throws SQLException {
		int count = 1;
		int schemaNumber;
		String temp = null;
		ArrayList<String> schemaName = new ArrayList<String>();
		statement = database.createStatement();
		String query = "select schema_name from information_schema.schemata where schema_name != 'information_schema';";
		resultSet = queryExecutor(query);
		System.out.println("Enter the schema number to make modification");
		while (resultSet.next()) {
			schemaName.add(resultSet.getString("schema_name"));
			System.out.println(count + ":" + resultSet.getString("schema_name"));
			count++;
		}
		schemaNumber = in.nextInt();
		while(schemaNumber > schemaName.size())
		{
			System.out.println("Enter the correct schema number to make modification");
			schemaNumber = in.nextInt();				
		}
		count = 1;
		for (String s : schemaName) {
			if (schemaNumber == count) {
				schema = s;
				temp = "use " + s + ";";
			}
			count++;
		}

		statement.execute(temp);
	}
	
	//method to set the schema before running the query
	void setschema(Connection database,String schema)
	{
		try {
			statement = database.createStatement();
			statement.execute(schema);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error occurred while setting schema");
		}
	}
	
	
	//method to reconcilepayments in the payments and orders table
	void reconcilePayments(Connection database) {
		try {
			
			setschema(database,schema);
			/*if (schema.isEmpty()) {
				setSchema(database);
			}*/
			boolean check1 = columnChecker(database, "payments", "orderNumber");
			if (!check1) {
				/*if (schema.isEmpty()) {
					setSchema(database);
				}*/
				setschema(database,schema);
				String dropConstraints = "SELECT constraint_name FROM information_schema.key_column_usage where table_name = 'payments' and constraint_schema = 'tshanmugam';";
				resultSet = queryExecutor(dropConstraints);
				while (resultSet.next()) {
					if (!(resultSet.getString("constraint_name").equalsIgnoreCase("PRIMARY"))) {
						String query1 = "ALTER TABLE payments drop foreign key "
								+ resultSet.getString("constraint_name") + ";";
						resultChecker = columnModifier(database, query1);
					}
				}

				String dropPrimaryQuery = "alter table payments drop primary key;";
				resultChecker = columnModifier(database, dropPrimaryQuery);

				String alterPayments = "ALTER TABLE payments ADD COLUMN (orderNumber int);";
				resultChecker = columnModifier(database, alterPayments);
				if (resultChecker) {
					String fKeyAdd = "ALTER TABLE payments ADD FOREIGN KEY (orderNumber) REFERENCES orders(orderNumber);";
					resultChecker = columnModifier(database, fKeyAdd);
					fKeyAdd = "ALTER TABLE payments ADD FOREIGN KEY (customerNumber) REFERENCES customers(customerNumber);";
					resultChecker = columnModifier(database, fKeyAdd);
					if (resultChecker) {
						System.out.println("Added orderNumber as foreign key in payments table");
						String uniqueKey = "alter table payments add unique  key(customerNumber,checkNumber ,orderNumber);";
						resultChecker = columnModifier(database, uniqueKey);
						updatePayments();
					}
				}
			} 
			
			else {
				System.out.println("orderNumber column already exists in payment table");
				System.out.println("Proceeding with the syncing");
				updatePayments();
			}

			boolean check2 = columnChecker(database, "orders", "paymentStatus");
			if (!check2) {
				String alterOrders = "alter table orders add column (paymentStatus varchar(25));";
				resultChecker = columnModifier(database, alterOrders);
				if (resultChecker) {
					System.out.println("Added paymentStatus column in orders table to track payment easily");
					updatePaymentStatus();
				}

			} else {
				System.out.println("payment status already exist in orders table");
				System.out.println("proceeding with the syncing");
				updatePaymentStatus();
			}

		} catch (SQLException e) {
			System.out.println("Error occcurred while reconciling payments");
		}
	}
	
	//method to check whether the column exist or not
	private Boolean columnChecker(Connection connection, String tableName, String columnName) {
		// TODO Auto-generated method stub
		try {
			DatabaseMetaData md = connection.getMetaData();
			resultSet = md.getColumns("tshanmugam", null, tableName, columnName);
			if (resultSet.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return false;
		}

	}

	//method to update the given orders in the payment table
	Boolean payOrder(Connection database, float amount, String cheque_number, ArrayList<Integer> orders) {

		String totalOrders = "";
		for (int i : orders) {
			totalOrders += Integer.toString(i) + ",";
		}
		try {
			setschema(database,schema);
			/*if (schema.isEmpty()) {
				setSchema(database);
			}*/
			boolean check1 = columnChecker(database, "payments", "orderNumber");
			if (check1) {
				statement = database.createStatement();
				String substring = totalOrders.substring(0, totalOrders.length() - 1);
				String query1 = "select count(distinct(orders.customerNumber)) as customerCount from orders inner join orderdetails on orders.orderNumber=orderdetails.orderNumber where orders.orderNumber in ("
						+ substring + ");";
				resultSet = queryExecutor(query1);
				resultSet.next();
				
				if (Integer.parseInt(resultSet.getString("customerCount")) == 1)
				{
				String query = "select count(distinct(orderdetails.orderNumber)) as ordersCount from orders inner join orderdetails on orders.orderNumber=orderdetails.orderNumber where orders.orderNumber in ("
						+ substring + ");";
				resultSet = queryExecutor(query);
				resultSet.next();
				if (Integer.parseInt(resultSet.getString("ordersCount")) == orders.size()) {
					query = "select sum( orderdetails.quantityOrdered * orderdetails.priceEach ) as orderTotal from orders inner join orderdetails  on orders.orderNumber = orderdetails.orderNumber where orders.orderNumber in ("
							+ substring + ");";
					resultSet = queryExecutor(query);
					resultSet.next();
					if (Float.parseFloat(resultSet.getString("orderTotal")) == amount) {
						orderList = new ArrayList<Order>();
						query = "select orders.customerNumber, orders.orderNumber ,sum( orderdetails.quantityOrdered * orderdetails.priceEach ) as orderTotal from orders inner join orderdetails  on orders.orderNumber=orderdetails.orderNumber where orders.orderNumber in ("
								+ substring + ") group by orders.orderNumber";
						resultSet = queryExecutor(query);
						while (resultSet.next()) {
							order = new Order(Integer.parseInt(resultSet.getString("customerNumber")),
									Integer.parseInt(resultSet.getString("orderNumber")),
									Float.parseFloat(resultSet.getString("orderTotal")));
							orderList.add(order);
						}
						updateOrders(database, orderList, cheque_number);
						return true;
					}
					else
					{

						System.out.println("Amount doesn't match with the orders value");
						return false;
	
					}
				} else {
					
					System.out.println("Some orders doesn't exist in the database");
					return false;
				}
			}
			else {
				System.out.println("Customer cannot pay for other customer orders");
			}
			} else {

				if (checkLoop == 0) {
					checkLoop++;
					System.out.println("Reconciling database before updating the order details");
					reconcilePayments(database);
					payOrder(database, amount, cheque_number, orders);
				}
				return false;

			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		System.out.println("Error occured while updating payment for the given orders");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Error occured while updating payment for the given orders");
		}

		return false;
	}

	//method to insert table to the payments table
	boolean updateOrders(Connection database, ArrayList<Order> orderList, String cheque_number) {
		boolean check1 = columnChecker(database, "payments", "orderNumber");
		if (check1) {
			for (Order order : orderList) {
				String insertQuery = "INSERT INTO payments VALUES (?, ?, current_date(), ?,?)";
				PreparedStatement p;
				try {
					p = database.prepareStatement(insertQuery);
					p.setInt(1, order.getCustomerNumber());
					p.setString(2, cheque_number);
					p.setFloat(3, order.getOrderTotal());
					p.setInt(4, order.getOrderNumber());
					p.executeUpdate();
					System.out.println("Inserted order "+order.getOrderNumber() +" in payments table");
					String query = "update orders set paymentStatus = 'paid' where orders.orderNumber ="
							+ order.getOrderNumber() + ";";
					dataModifier(query);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					System.out.println("Data Already updated");
				
				}

			}
		}
		return true;
	}

	//method to retrieve unpaid orders
	ArrayList<Integer> unpaidOrders(Connection database) {
		try {
			statement = database.createStatement();
			setschema(database,schema);
			/*if (schema.isEmpty()) {
				setSchema(database);
			}*/
			boolean check1 = columnChecker(database, "payments", "orderNumber");
			if (!check1) {
				String unpaidOrders = "select  totalOrders.orderNumber from (select orders.orderNumber , sum(orderdetails.priceEach *  orderdetails.quantityOrdered) as amount,orders.customerNumber, orders.status from orders  inner join  orderdetails on orders.orderNumber = orderdetails.orderNumber group by orders.orderNumber order by orders.customerNumber) as totalOrders left join payments on totalOrders.amount = payments.amount  where  totalOrders.status not in ( 'Cancelled' ,'Disputed' ) and  payments.paymentDate is null order by totalOrders.status;";
				resultSet = queryExecutor(unpaidOrders);
				rowCount = 0;
				unpaidOrdersList = new ArrayList<Integer>();
				while (resultSet.next()) {
					rowCount++;
					int orderNumber = Integer.parseInt(resultSet.getString("orderNumber"));
					unpaidOrdersList.add(orderNumber);
				}
				System.out.println("Total unpaid orders:" + rowCount);
			} else {
				String unpaidOrders = "select orders.orderNumber from orders left join payments on orders.orderNumber = payments.orderNumber where payments.orderNumber is null;";
				resultSet = queryExecutor(unpaidOrders);
				rowCount = 0;
				unpaidOrdersList = new ArrayList<Integer>();
				while (resultSet.next()) {
					rowCount++;
					int orderNumber = Integer.parseInt(resultSet.getString("orderNumber"));
					unpaidOrdersList.add(orderNumber);
				}
				System.out.println("Total unpaid orders:" + rowCount);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error occured while retrieving unpaid orders");
		}
		return unpaidOrdersList;
	}

	//method to retrieve from the unknownpayments
	ArrayList<String> unknownPayments(Connection database) {

		try {
			statement = database.createStatement();
			setschema(database,schema);
			/*if (schema.isEmpty()) {
				setSchema(database);
			}*/
			boolean check1 = columnChecker(database, "payments", "orderNumber");
			if (!check1) {
				String checkNumbers = "select payments.checkNumber from (select orders.orderNumber , sum(orderdetails.priceEach *  orderdetails.quantityOrdered) as amount,orders.customerNumber, orders.status from orders  inner join  orderdetails on orders.orderNumber = orderdetails.orderNumber group by orders.orderNumber order by orders.customerNumber) as totalOrders right join payments on totalOrders.amount = payments.amount  where totalOrders.orderNumber is null order by totalOrders.status;";
				resultSet = queryExecutor(checkNumbers);
				rowCount = 0;
				unUsedChequeList = new ArrayList<String>();
				while (resultSet.next()) {
					rowCount++;
					String chequeNumber = resultSet.getString("checkNumber");
					unUsedChequeList.add(chequeNumber);
				}
				System.out.println("Total un used cheque number:" + rowCount);
			} else {
				String checkNumbers = "select  payments.checkNumber from payments where orderNumber is null;";
				resultSet = queryExecutor(checkNumbers);
				rowCount = 0;
				unUsedChequeList = new ArrayList<String>();
				while (resultSet.next()) {
					rowCount++;
					String chequeNumber = resultSet.getString("checkNumber");
					unUsedChequeList.add(chequeNumber);
				}
				System.out.println("Total un used cheque number:" + rowCount);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error occured while retrieving unknown payments orders");
		}
		return unUsedChequeList;
	}

}
