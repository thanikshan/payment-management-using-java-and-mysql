//package Assignment6;

public class Order {

	private int orderNumber;
	private float orderTotal;
	private int customerNumber;


	public Order() {
		super();
	}



	public Order( int customerNumber ,int orderNumber,float orderTotal ) {
		super();
		this.orderNumber = orderNumber;
		this.orderTotal = orderTotal;
		this.customerNumber = customerNumber;
	}



	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public float getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(float orderTotal) {
		this.orderTotal = orderTotal;
	}


	
	

}
